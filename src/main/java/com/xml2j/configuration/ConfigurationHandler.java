package com.xml2j.configuration;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class ConfigurationHandler extends org.xml.sax.helpers.DefaultHandler {

	private static Logger logger = LoggerFactory.getLogger(ConfigurationHandler.class);

	private final Xml2jConfiguration configuration;
	
	public ConfigurationHandler(Xml2jConfiguration configuration) {
		this.configuration = configuration;
	}
	
	public Xml2jConfiguration getConfiguration() {  
		return this.configuration;
	}

	public void parseDocument(final String document) throws SAXException, IOException, ParserConfigurationException {
		// get a factory
		SAXParserFactory spf = SAXParserFactory.newInstance();

		// get a new instance of parser
		SAXParser sp = spf.newSAXParser();

		// parse the file and also register this class for call backs
		sp.parse(document, this);
	}

	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes atts) throws SAXException {
		try {
			if (qName.equals("xml2j-generator")) {
				//
			} else if (qName.equals("domain")) {
				Xml2jDomain domain = new Xml2jDomain();
				// optional BASE
				String base = atts.getValue("base");
				domain.base = base != null ? base.replace('-', '_') : null;
				// optional NAME
				String name = atts.getValue("name");
				domain.name = name != null ? name.replace('-', '_') : null;
				// optional DESCRIPTION	
				String description = atts.getValue("description");
				domain.description = description != null ? description.replace('-', '_') : null;

				domain.input_path = atts.getValue("input-path");
				configuration.add(domain);

			} else if (qName.equals("module")) {
				Xml2jModule module = new Xml2jModule();
				// required NAME
				module.name = atts.getValue("name").replace('-', '_');;
				module.description = atts.getValue("description");
				module.input_path = atts.getValue("input-path");
				module.output_path = atts.getValue("output-path");
				configuration.getDomain().add(module);

			} else if (qName.equals("interface")) {
				Xml2jInterface intf = new Xml2jInterface();
				intf.name = atts.getValue("name");
				intf.message_handler_root = atts.getValue("message-handler-root");
				intf.message_handler_name = atts.getValue("message-handler-name");
				intf.root_type_rename = atts.getValue("root-type-rename");
				intf.message_handler_processor = atts.getValue("message-handler-processor");
				intf.message_handler_runnable = atts.getValue("message-handler-runnable");
				intf.message_handler_application_task = atts.getValue("message-handler-application-task");
				intf.message_handler_application = atts.getValue("message-handler-application");
				configuration.getDomain().current().add(intf);
			}

		} catch(NullPointerException e) {
			logger.error("NullPointerException: ConfigurationHandler.startElement");
			System.exit(-1);
		}
	}
}
