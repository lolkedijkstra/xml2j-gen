package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import com.xml2j.configuration.Xml2jInterface;
import com.xml2j.configuration.Xml2jModule;

public class TransformationSteps {
	
	/* input parameters for transformations */
	static final private String PACKAGE_NAME = "package-name";
//	static final private String DOMAIN_NAME = "domain-name";
	static final private String MODULE_NAME = "module-name";
	static final private String MODULE_PACKAGE = "module-package";
//	static final private String MODULE_DESCRIPTION = "module-description";

	static final private String MESSAGE_HANDLER_ROOT = "message-handler-root";
	static final private String MESSAGE_HANDLER_NAME = "message-handler-name";
	static final private String MESSAGE_HANDLER_PACKAGE = "message-handler-package";
	static final private String ROOT_TYPE_RENAME = "root-type-rename";

	static final private String PROCESSOR_PACKAGE = "processor-package";
	static final private String PROCESSOR_NAME = "processor-name";

	static final private String APPLICATION_NAME = "application-name";
	static final private String APPLICATION_PACKAGE = "application-package";
	static final private String APPLICATION_TASK_NAME = "application-task-name";
	static final private String APPLICATION_TASK_PACKAGE = "application-task-package";
	static final private String NAMESPACE_AWARE = "namespace-aware";
	static final private String RUNNABLE_NAME = "runnable-name";
	static final private String RUNNABLE_PACKAGE = "runnable-package";

	static final private String PRINTING = "with-printing";
	static final private String SOURCE_PATH = "source-path";
	static final private String AUTHOR = "author";
	static final private String CUSTOMHEADER = "custom-header";

	static final private String SERIALIZATION = "with-serialization";
	static final private String SERIALVERSION_UID = "serialversion-uid";

	static final private String XML2J_VERSION = "xml2j-version";
	static final private String XML2J_VERSION_NUMBER = Xml2jGenerator.VERSION;
	
	private List<TransformationStep> codeGenerationSteps = new ArrayList<>();

	private final Xml2jModule module;
	private final Xml2jInterface iface;

	private final Pair[] paramsStep1;
	private final Pair[] paramsStep2;
	private final Pair[] paramsStep3;
	private final Pair[] paramsStepCodeGeneration;
	
	public TransformationSteps(Xml2jModule m, Xml2jInterface i, String moduleRoot, String modulePackage, CLIParser cli) {
		this.module = m;
		this.iface  = i;
		
		// @formatter:off
		paramsStep1 = new Pair[] {};
		
		paramsStep2 = new Pair[] { 
				new Pair(PACKAGE_NAME, modulePackage), new Pair(MODULE_NAME, module.name) 
			};
		
		paramsStep3 = new Pair[] { 
				new Pair(MESSAGE_HANDLER_ROOT, iface.message_handler_root),
				new Pair(MESSAGE_HANDLER_NAME, iface.message_handler_name),
				new Pair(MESSAGE_HANDLER_PACKAGE, modulePackage + ".handlers"),
				new Pair(ROOT_TYPE_RENAME, iface.root_type_rename) 
			};
		
		paramsStepCodeGeneration = new Pair[] {
				new Pair(MODULE_NAME, module.name),
				new Pair(MODULE_PACKAGE, modulePackage),
				new Pair(PROCESSOR_NAME, iface.message_handler_processor),
				new Pair(PROCESSOR_PACKAGE, modulePackage + ".processor"),
				new Pair(RUNNABLE_NAME, iface.message_handler_runnable),
				new Pair(RUNNABLE_PACKAGE, modulePackage + ".runnable"),
				new Pair(APPLICATION_TASK_NAME, iface.message_handler_application_task),
				new Pair(APPLICATION_TASK_PACKAGE, modulePackage + ".application"),
				new Pair(NAMESPACE_AWARE, iface.namespace_aware),
				new Pair(APPLICATION_NAME, iface.message_handler_application),
				new Pair(APPLICATION_PACKAGE, modulePackage + ".application"),
				new Pair(MESSAGE_HANDLER_NAME, iface.message_handler_name),
				new Pair(MESSAGE_HANDLER_PACKAGE, modulePackage + ".handlers"),
				new Pair(SOURCE_PATH, moduleRoot + "/" + module.output_path),
				new Pair(PRINTING, cli.hasPrint() ? "1" : "0"),
				new Pair(SERIALIZATION, cli.hasSerialize() ? "1" : "0"),
				new Pair(SERIALVERSION_UID, format("%dL", cli.getSerialize())),
				new Pair(AUTHOR, cli.getAuthor()),
				new Pair(CUSTOMHEADER, Header.customHeader),
				new Pair(XML2J_VERSION, XML2J_VERSION_NUMBER)
			};
		// @formatter:on
		
		codeGenerationSteps.add(new TransformationStep(Step.FIRST, "xml2j1", paramsStep1));
		codeGenerationSteps.add(new TransformationStep(Step.SECOND, "xml2j2", paramsStep2));
		codeGenerationSteps.add(new TransformationStep(Step.THIRD, "xml2j3", paramsStep3));
		codeGenerationSteps.add(new TransformationStep(Step.GENERATE_JAVA, "xml2j4", paramsStepCodeGeneration));
	}
	
	
	List<TransformationStep> getCodeGenerationSteps() {
		return this.codeGenerationSteps;
	}
	
}
