package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import org.apache.commons.cli.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.String.format;

/**
 * Command line options.
 **/

public class CLIParser {
	private static Logger logger = LoggerFactory.getLogger(CLIParser.class);

	private Options options = new Options();
	private CommandLineParser parser = new DefaultParser();
	private HelpFormatter helpFormatter = new HelpFormatter();
	private CommandLine commandLine = null;

	private static final String DEFAULT_AUTHOR = "XML2J-GEN";

	public void printHelp() {
		helpFormatter.printHelp("java -jar xml2j.jar [options]", options);
	}

	public CLIParser() {
	// @formatter:off
		options
			.addOption("p", "print", false, "generate printing methods (defaults to none)")
			.addOption("l", "license", false, "print license details").addOption("?", "help", false, "prints help")
			.addOption("w", "workingdirectory", true, "working directory either absolute or relative to xml2j.jar's directory")
			.addOption("h", "header", true, "insert custom header")
			.addOption("s", "serialVersionUID", true, "serialVersionUID with (long) ID (defaults to 1L)")
			.addOption("v", "verbose", false, "verbose")
			.addOption("i", "intermediate", false, "output intermediate results")
			.addOption("res", "resources", false, "generate log4j.propeties file in resources folder")
			.addOption("rm", "remove", false, "remove old code before generating code")
			.addOption("version", "version", false, "prints version")
			.addOption("pom", "pom", false, "generate POM files (defaults to none)")
			.addOption("a", "author", true, format("author (defaults to %s)", DEFAULT_AUTHOR))
			.addOption("c", "configuration", true, "(mandatory) configuration file used by generator");
    // @formatter:on
	}

	private boolean isOptionSet(String opt) {
		if (commandLine == null)
			throw new NullPointerException("Commandline has not been parsed. Must use parse first");

		return commandLine.hasOption(opt);
	}

	private String getValue(String name) {
		return isOptionSet(name) ? commandLine.getOptionValue(name) : null;
	}

	private boolean serialVersionNaN = false;
	private long serialDefaultVersionUID = 1L;
	private long serialVersionUID = 0L; // if 0L -> not set

	public long getSerialize() {
		if (serialVersionUID != 0L)
			return serialVersionUID;

		if (serialVersionNaN)
			return serialDefaultVersionUID;

		if (hasSerialize()) {
			try {
				serialVersionUID = Long.parseLong(getValue("s"));
				return serialVersionUID;
			} catch (NumberFormatException e) {
				logger.warn("serialVersionUID NaN: Provided \"{}\", defaulting to 1L.", getValue("s"));
				serialVersionNaN = true;
			}
		}
		
		return serialDefaultVersionUID;
	}

	// @formatter:off
    public String getAuthor() { return hasAuthor() ? getValue("a") : DEFAULT_AUTHOR; }
    public String getConfigfileName() { return getValue("c"); }
    public String getHeader() { return getValue("h"); }
    public String getWorkingDirectory() { return getValue("w"); }
 
    public boolean hasPrintVersion() { return isOptionSet("version"); }
    public boolean hasPrintLicense() { return isOptionSet("l"); }
    public boolean hasPrintHelp() { return isOptionSet("?"); }
    public boolean hasAuthor() { return isOptionSet("a");}
    public boolean hasConfig() { return isOptionSet("c"); }
    public boolean hasIntermediate() { return isOptionSet("i"); }
    public boolean hasHeader() { return isOptionSet("h"); }
    public boolean hasPom() { return isOptionSet("pom"); }
    public boolean hasResources() { return isOptionSet("res"); }
    public boolean hasPrint() { return isOptionSet("p"); }
    public boolean hasRemove() { return isOptionSet("rm"); }
    public boolean hasSerialize() { return isOptionSet("s"); }
    public boolean hasVerbose() { return isOptionSet("v"); }
    public boolean hasWorkingDirectory() { return isOptionSet("w"); }
    
    public void parse(String[] args) throws ParseException { commandLine = parser.parse(options, args); }
    // @formatter:on

}