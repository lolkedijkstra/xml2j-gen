package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import com.xml2j.configuration.Xml2jConfiguration;
import com.xml2j.configuration.Xml2jDomain;

class Xml2jGeneratorSettings {
    private String base;
    private String domainname;
    private String domainpackage;
    private String domaindescription;
    
    private Xml2jConfiguration configuration;
    private String workingDirectory;

    private static final String BASE = "com.xml2j";

    Xml2jGeneratorSettings(final Xml2jConfiguration configuration, String workingDirectory) {
        this.configuration = configuration;
        this.workingDirectory = workingDirectory;
        this.setNames();
    }

    public String getBase() {
    	return this.base;
    }
    
    public String getWorkingDirectory() {
    	return this.workingDirectory;
    }
    
    public String getDomainName() {
    	return this.domainname;
    }

    public String getDomainDescription() {
		return this.domaindescription;
	}

    public String getDomainPackage() {
    	return this.domainpackage;
    }
    
    public Xml2jConfiguration getConfiguration() {
    	return this.configuration;
    }
    
    private void setNames() {
        Xml2jDomain domain = configuration.getDomain();

        /* create package names */
        base = domain.base != null && !domain.base.isEmpty() ? domain.base : BASE;
        domainname = domain.name != null && !domain.name.isEmpty() ? (domain.name) : "";
        domainpackage = base + "." + domainname;
        domaindescription = domain.description;
    }

}