package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xml2j.configuration.Xml2jDomain;
import com.xml2j.configuration.Xml2jModule;

public class Resources {
	
	private static Logger logger = LoggerFactory.getLogger(Resources.class);

	static void generateResourcesForDomain(final String workingDirectory, final Xml2jDomain d, final boolean hasVerbose) {
		d.modules().forEach( m -> { generateResourcesForModule(workingDirectory, m, hasVerbose); } );
	}
	
	static void generateResourcesForModule(final String workingDirectory, final Xml2jModule m, final boolean hasVerbose) {
		// copy resources folder to destination
		final String sourcePath = workingDirectory + "/" + m.output_path;
		final String resourcePath = sourcePath.substring(0, sourcePath.lastIndexOf("java")).concat("resources");
		
		Path dstPath = Paths.get(resourcePath);
		Path src = Paths.get("./resources/log4j.properties");
		try {
			if (!Files.exists(dstPath))
				Files.createDirectory(dstPath);
			
			Path dst = Paths.get(resourcePath.concat("/log4j.properties"));
			
			if (hasVerbose) {
				logger.info("Generating log4j.properties for module {} at {}", m.name, dst);
			}

			Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}
	}	

}
