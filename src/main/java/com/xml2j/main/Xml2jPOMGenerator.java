package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xml2j.configuration.Xml2jDomain;
import com.xml2j.configuration.Xml2jModule;
import static com.xml2j.files.IOUtils.writeFile;

public class Xml2jPOMGenerator {
	private static Logger logger = LoggerFactory.getLogger(Xml2jPOMGenerator.class);

	// @formatter:off
	private static final String DEFAULT_POM = "pom-single";
	private static final String MASTER_POM  = "pom-master";
	private static final String MODULE_POM  = "pom-module";
	// @formatter:on
	
	private String workingDirectory;
	private Xml2jGeneratorSettings generatorSettings;
	private boolean verbose;

	public Xml2jPOMGenerator(Xml2jGeneratorSettings generatorSettings, boolean verbose) {
		this.verbose = verbose;
		this.generatorSettings = generatorSettings;
		this.workingDirectory = generatorSettings.getWorkingDirectory();
	}
	
	private String print(final Pair[] params) {
		StringBuilder sb = new StringBuilder();
		for (Pair p : params) {
			sb.append("\n\t").append(p.getKey()).append(':').append(p.getValue());			
		}
		return sb.toString();
	}
	
	private Pair[] getParamForPOM(Xml2jModule module) {
		// set parameters for pom generation 
		final String modulePackage = generatorSettings.getDomainPackage() + "." + module.name;
		final String mainClass = module.interfaces().listIterator().next().message_handler_application;
		
		final Pair[] params = {
			new Pair("group-id", modulePackage.substring(0, modulePackage.lastIndexOf('.'))),
			new Pair("domain-name", generatorSettings.getDomainName()),
			new Pair("module-name", module.name),
			new Pair("module-description", module.description),
			new Pair("module-package", modulePackage),
			new Pair("source-path", module.output_path),
			new Pair("application-name", mainClass),
			new Pair("application-package", modulePackage + ".application"),
			new Pair("xml2j-version", Xml2jGenerator.VERSION)
		};

		if (this.verbose) {
			logger.info(print(params));
		}

		return params;
	}
	
	public Pair[] getParamsForMasterPOM() {
		final Pair[] params = {
			new Pair("group-id", generatorSettings.getDomainPackage()),
			new Pair("domain-name", generatorSettings.getDomainName()),
			new Pair("domain-description", generatorSettings.getDomainDescription()),
			new Pair("xml2j-version", Xml2jGenerator.VERSION)
		};

		if (this.verbose) {
			logger.info(print(params));
		}

		return params;
	}

	
	private void generateMasterPom(Xml2jDomain domain) {
		final String empty = "<?xml version=\"1.0\" encoding=\"utf-8\"?><modules>";

		StringBuilder stringBuilder = new StringBuilder(empty);
		domain.modules().forEach( m -> {
			stringBuilder.append("<module xmlns=\"http://maven.apache.org/POM/4.0.0\">").append(m.name).append("</module>");
		});
		stringBuilder.append("</modules>");

		try {
			Transformation t = new Transformation(MASTER_POM, getParamsForMasterPOM());

			final String xml = stringBuilder.toString();
			final String pom = workingDirectory + "/pom.xml";
			try (InputStream input = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
				 ByteArrayOutputStream output = t.executeStep(input, null)) {

				logger.info("Creating (master) POM file: {}", pom);
				writeFile(output.toByteArray(), pom);
			}

		} catch (Exception e) {
			logger.error("Error generating master POM. {}", e.getMessage());
			System.exit(-1);
		}
	}

	private void generateModulePom(Xml2jModule module, boolean hierchical) {
		final Step step = hierchical ? Step.MODULE_POM : Step.DEFAULT_POM;
		final String pom = hierchical ? MODULE_POM : DEFAULT_POM;

		if (module.interfaces().isEmpty()) {
			logger.warn("Module has no interfaces. POM cannot be generated for module {}.", module.name);
			return;
		}

		final String empty = "<?xml version=\"1.0\" encoding=\"utf-8\"?><document xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"/>";

		if (module.interfaces().size() > 1) {
			logger.warn("Module {} has more than 1 interface. Settings of first interface are used for module POM.", module.name);
		}

		Transformation t = new Transformation(pom, getParamForPOM(module));
		try (InputStream input = new ByteArrayInputStream(empty.getBytes(StandardCharsets.UTF_8));
			ByteArrayOutputStream output = t.executeStep(input, null)) {

			String pomFile = null;
			String type = null;
			switch (step) {
			case MODULE_POM:
				pomFile = workingDirectory + "/" + module.name + "/pom.xml";
				type = "(module)";
				break;
			case DEFAULT_POM:
				pomFile = workingDirectory + "/pom.xml";
				type = "(default)";
				break;
			default:
				break;
			}
			logger.info("Creating {} POM file: {}", type, pomFile);
			writeFile(output.toByteArray(), pomFile);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void generatePomFiles(final Xml2jDomain d) {
		final boolean hierarchical = d.modules().size() > 1;

		if (hierarchical) 
			generateMasterPom(d);
		
		d.modules().forEach( m -> { generateModulePom(m, hierarchical); } );
	}
	
}
