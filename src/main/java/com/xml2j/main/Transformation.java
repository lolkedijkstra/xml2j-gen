package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class Transformation {
	private String styleSheet;
	private final Pair[] params;

	private static Logger logger = LoggerFactory.getLogger(Transformation.class);
	
	Transformation(final String xslSheet, final Pair[] params) {
		assert(xslSheet != null && params != null);
		this.styleSheet = Xml2jGenerator.HOME + "/xslt/" + xslSheet + ".xsl";
		this.params = params;
	}


	ByteArrayOutputStream executeStep(final InputStream input, final String inSystemId) {
		try {
			byte[] xb = raw(styleSheet).toByteArray();
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			InputStream currentXsl = new ByteArrayInputStream(xb);
			execute(input, output, currentXsl, params, inSystemId);
			return output;
		} catch (Exception e1) {
			logger.error(e1.getMessage());
		}
		return null;
	}

	private ByteArrayOutputStream raw(final String input) throws IOException {
		if (input == null)
			throw new IllegalArgumentException("parameter input cannot be null");

		try (InputStream is = new FileInputStream(styleSheet)) {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();

			byte[] buf = new byte[1024];
			int n;
			while ((n = is.read(buf)) >= 0) {
				bos.write(buf, 0, n);
			}
			return bos;

		} catch (Exception e) {
			// swallow: return null in case of failure
		} 
		return null;
	}
	
//	Java 1.5 code
//	
//	private ByteArrayOutputStream raw(final String input) throws IOException {
//		if (input == null)
//			throw new IllegalArgumentException("parameter input cannot be null");
//
//		InputStream is = new FileInputStream(styleSheet);
//
//		ByteArrayOutputStream bos = null;
//		try {
//			bos = new ByteArrayOutputStream();
//
//			byte[] buf = new byte[1024];
//			int n;
//			while ((n = is.read(buf)) >= 0) {
//				bos.write(buf, 0, n);
//			}
//			return bos;
//
//		} catch (Exception e) {
//			// swallow: return null in case of failure
//		} finally {
//			try {
//				is.close();
//			} catch (IOException e) {
//				logger.warn(e.getMessage());
//			}
//		}
//		return bos;
//	}


	/**
	 * Set runtime parameters in the XSLT transformer
	 * 
	 * @param t
	 *            transformer
	 * @param p
	 *            parameters
	 */
	private void setParameters(final Transformer t, final Pair[] p) {
		for (int i=0; i != p.length; i++ ) {
			t.setParameter(p[i].getKey(), p[i].getValue());			
		}
	}

	private void execute(final InputStream input, final OutputStream output, final InputStream style, final Pair[] params, final String inSystemId) {
		try {
			TransformerFactory f = TransformerFactory.newInstance();
			Transformer t = f.newTransformer(new StreamSource(style));
			t.reset();

			setParameters(t, params);

			Source source = new StreamSource(input);
			if (inSystemId != null)
				source.setSystemId(inSystemId);

			t.transform(source, new StreamResult(output));
		} catch (TransformerException e) {
			logger.error("Configuration error. Check configuration file.");
			System.exit(-1);
		}
	}

}