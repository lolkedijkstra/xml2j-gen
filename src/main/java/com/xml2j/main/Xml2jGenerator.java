package com.xml2j.main;
/********************************************************************************
Copyright 2016-2021 Lolke B. Dijkstra

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION 
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Project root: https://sourceforge.net/projects/xml2j/
********************************************************************************/

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import com.xml2j.configuration.ConfigurationHandler;
import com.xml2j.configuration.ConfigurationValidator;
import com.xml2j.configuration.Xml2jConfiguration;
import com.xml2j.configuration.Xml2jDomain;
import com.xml2j.configuration.Xml2jInterface;
import com.xml2j.configuration.Xml2jModule;

import static com.xml2j.files.IOUtils.closeStream;
import static com.xml2j.files.IOUtils.fileDelRecursive;
import static com.xml2j.files.IOUtils.toInputStream;
import static java.lang.String.format;

public class Xml2jGenerator {
	public static final String VERSION = "2.5.5";
	private static final String SAX_PARSER_FACTORY = "org.apache.xerces.jaxp.SAXParserFactoryImpl";

	private static Logger logger = LoggerFactory.getLogger(Xml2jGenerator.class);
	private CLIParser cli;

	// @formatter:off
	private static String LICENSE = "\n\r---------------------------------------------------------------------------------------"
			+ "\nCopyright 2016-2021 Lolke B. Dijkstra " 
			+ "\nPermission is hereby granted, free of charge, to any person obtaining a copy"
			+ "\nof this software and associated documentation files (the \"Software\"), to deal"
			+ "\nin the Software without restriction, including without limitation the rights to "
			+ "\nuse, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of "
			+ "\nthe Software, and to permit persons to whom the Software is furnished to do so, " 
			+ "\nsubject to the following conditions:\n"

			+ "\nThe above copyright notice and this permission notice shall be included in all copies" 
			+ "\nor substantial portions of the Software.\n"

			+ "\nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,"
			+ "\nINCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A"
			+ "\nPARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT"
			+ "\nHOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION "
			+ "\nOF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE " 
			+ "\nOR THE USE OR OTHER DEALINGS IN THE SOFTWARE."

			+ "\nhttps://gitlab.com/lolkedijkstra" + "\nemail: lolkedijkstra@gmail.com"
			+ "\n---------------------------------------------------------------------------------------\n";

	/* CUSTOM HEADER */
	private static String header = "\n	This software was generated using XML2J code generator. "
			+ "\n see: https://sourceforge.net/projects/xml2j/"
			+ "\n  -----------------------------------------------------------------------------\n";
	// @formatter:on


	Xml2jGenerator(CLIParser cli, Xml2jConfiguration configuration) throws SAXException, IOException, ParserConfigurationException {
		this.cli = cli;
		this.configuration = configuration;
		this.domain = configuration.getDomain();
		this.generatorSettings = new Xml2jGeneratorSettings(configuration, workingDirectory);
		System.setProperty("javax.xml.parsers.SAXParserFactory", SAX_PARSER_FACTORY);
	}
	
	private Xml2jConfiguration configuration;
	
    private Xml2jDomain domain;
    private Xml2jModule module;
	private String moduleRoot;
	private String modulePackage;
	private Xml2jGeneratorSettings generatorSettings;

	/* basic settings */
	static final String XML2J_HOME = "XML2J_HOME";
	static final String HOME = System.getenv(XML2J_HOME);
	static final String SCHEMA = HOME + "/schema/xml2j.xsd";

	private static String configFileName;
	private static String workingDirectory = ".";

	public Xml2jGeneratorSettings getGeneratorSettings() {
		return this.generatorSettings;
	}
	
	public String getWorkingDirectory() {
		return workingDirectory;
	}
	

	private static void initialize(CLIParser cli) {
		workingDirectory = cli.getWorkingDirectory();
		if (workingDirectory == null)
			workingDirectory = ".";

		if (!workingDirectory.equals(".")) {
			configFileName = cli.getWorkingDirectory() + "/" + cli.getConfigfileName();
		} else {
			configFileName = cli.getConfigfileName();
		}

		if (cli.hasVerbose()) {
			logger.info("workingDirectory: {}", workingDirectory);
			logger.info("configFileName: {}", configFileName);
		}

		if (cli.hasHeader()) {
			Header.readHeader(workingDirectory + '/'+ cli.getHeader());
		}

	}

	private static void validateConfiguration(String configFileName) throws FileNotFoundException, SAXException, IOException {
		ConfigurationValidator validator = new ConfigurationValidator(SCHEMA);
		validator.validateDocument(configFileName);
	}

	private static Xml2jConfiguration parseConfiguration(String configFileName) throws SAXException, IOException, ParserConfigurationException {
		ConfigurationHandler handler = new ConfigurationHandler(new Xml2jConfiguration());
		handler.parseDocument(configFileName);
		return handler.getConfiguration();
	}


	private String getXml2jInterfaceXSDFileName(Xml2jInterface i) {
		final String name = module.input_path != null ? module.input_path : (domain.input_path != null ? domain.input_path : "schema");
		return moduleRoot
				+ "/" + name
				+ "/" + i.name;
	}

	private void generateCodeForDomain() {
		Xml2jDomain d = configuration.getDomain();
		
		if (cli.hasVerbose()) {
			d.print(System.out);
			System.out.println();
		}

		configuration.getDomain().modules().forEach( m -> { generateCodeForModule(m); } );
	}

	private void generateCodeForModule(final Xml2jModule m) {
		/* setting parameters for code generation */
		module = m;
		moduleRoot = workingDirectory + "/" + m.name;
		modulePackage = generatorSettings.getDomainPackage() + "." + module.name;

		logger.info("Generating code for module: " + module.name);
		
		/* remove old code? */
		if (cli.hasRemove()) {
			final String sourcePath = moduleRoot + "/" + m.output_path;
			logger.info("Removing directory: {}", sourcePath);
			fileDelRecursive(new File(sourcePath));
		}

		
		/* for all interfaces in module generate code */
		m.interfaces().forEach(i -> { generateCodeForInterface(i); });
	}

	private void generateCodeForInterface(final Xml2jInterface iface) {

		if (iface.message_handler_application != null
				&& iface.message_handler_runnable == null
				&& iface.message_handler_application_task == null)
			iface.message_handler_application_task = iface.message_handler_application + "Task";

		try {
			/* code transformation steps */
			final String schema = getXml2jInterfaceXSDFileName(iface);

			InputStream input = null;
			ByteArrayOutputStream output = null;
			
			TransformationSteps steps = new TransformationSteps(module, iface, moduleRoot, modulePackage, cli);
			List<TransformationStep> tsl = steps.getCodeGenerationSteps();
			
			for (int i=0; i!=tsl.size(); i++) {
				final TransformationStep ts = tsl.get(i);
				
				if (Step.FIRST == ts.getStep()) { 
					/* initialize input */
					input = new FileInputStream(schema);					
				} else {
					/* input is output previous step */
					input = toInputStream(output);
					closeStream(output);
				}
				
				/* perform transformation */
				output = performStep(ts, input, schema);
				
				if (cli.hasIntermediate()) {
					writeTransformationResultFile(iface.name, output.toByteArray(), ts.getStyleSheet());
				}

				closeStream(input);
				closeStream(output);
			}
			
		} catch (Exception e) {
			logger.error("Configuration error: reinstall. {}", e.getMessage());
			System.exit(-1);
		}
	}

	
	private ByteArrayOutputStream performStep(final TransformationStep step, final InputStream input, String systemId) {
		Transformation t = new Transformation(step.getStyleSheet(), step.getParams());
		return t.executeStep(input, systemId);
	}


	/**
	 * Write transformation steps to existing $HOME/temp/int folder.
	 * 
	 * pre-condition: $HOME/temp directory exists
	 * @param intName
	 *            name of the interface
	 * @param bytes
	 *            input
	 * @param step
	 *            step
	 */
	private static void writeTransformationResultFile(final String intName, final byte[] bytes, final String step) {
		try {
			final String tmpDir = HOME + "/temp";
			File folder = new File(tmpDir);
			if (!folder.exists()) {
				if(!folder.mkdir()) {
					logger.error("could not create {}", tmpDir);
				}
			}
			FileOutputStream fi = new FileOutputStream(format("%s/temp/%s-%s.xml", HOME, intName, step));
			fi.write(bytes);
			fi.flush();
			fi.close();
		} catch (IOException e) {
			logger.error("write intermediate: {}", e.getMessage());
		}
	}


	public static void main(final String[] args) {
		if (HOME == null) {
			logger.error("Configuration error. Missing environment variable: XML2J_HOME.");
			System.exit(-1);
		}

		CLIParser cli = new CLIParser();
		
		/* set command line options */
		try {
			cli.parse(args);
		} catch (Exception e) {
			logger.warn(e.getMessage());
			return;
		}

		/* exit conditions */
		if (cli.hasPrintVersion()) {
			System.out.println(VERSION);
			return;
		}

		if (cli.hasPrintLicense()) {
			System.out.println(LICENSE);
			return;
		}

		if (cli.hasPrintHelp() || !cli.hasConfig()) {
			cli.printHelp();
			return;
		}

		
		try {
			// initialization 
			logger.info("Initializing...");
			Xml2jGenerator.initialize(cli);

			logger.info("Validating configuration file...");
			Xml2jGenerator.validateConfiguration(configFileName);

			// load configuration 
			logger.info("Loading configuration ...");
			Xml2jConfiguration configuration = Xml2jGenerator.parseConfiguration(configFileName);
			Xml2jGenerator xml2jGenerator = new Xml2jGenerator(cli, configuration);

			// start transformation
			logger.info("Generating java code...");

			// generate code 
			xml2jGenerator.generateCodeForDomain();

			// POM files 
			if (cli.hasPom()) {
				logger.info("Generating POM files...");
				Xml2jPOMGenerator pg = new Xml2jPOMGenerator(xml2jGenerator.getGeneratorSettings(), cli.hasVerbose());
				pg.generatePomFiles(configuration.getDomain());
			}
			
			// resources
			if (cli.hasResources()) {
				logger.info("Generating resource files...");
				Resources.generateResourcesForDomain(xml2jGenerator.getWorkingDirectory(), configuration.getDomain(), cli.hasVerbose());
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			System.exit(-1);
		}
	}
}
